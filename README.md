##### Summary

The link provided in the test no longer works
> http://www.sainsburys.co.uk/webapp/wcs/stores/servlet/CategoryDisplay?listView=true&orderBy=FAVOURITES_FIRST&parent_category_rn=12518&top_category=12518&langId=44&beginIndex=0&pageSize=20&catalogId=10137&searchTerm=&categoryId=185749&listId=&storeId=10151&promotionId=#langId=44&storeId=10151&catalogId=10137&categoryId=185749&parent_category_rn=12518&top_category=12518&pageSize=20&orderBy=FAVOURITES_FIRST&searchTerm=&beginIndex=0&hideFilters=true

I gathered this was the link to the older version of the Sainsbury's website.  Going through the newer website I had discovered it was using JavaScript.  I had initially explored trying to scrape using a JavaScript engine, but then decided against it because solution would probably require external dependency PhantomJS and take more than 2 hours.  Instead I was looking for a similar page from another supermarket and found Morrisons page:

> https://groceries.morrisons.com/webshop/getCategories.do?tags=%7C105651%7C104268%7C113968%7C113902%7C154698&viewAllProducts=true&index=0&view=text

---
##### The system dependencies required by the project
* Java 8
* Maven 3.1.1+
##### To run tests simply execute the following from root of the project
```sh
    mvn test
```
##### To run the app with the defined link above simply execute the following from root of the project
```sh
mvn exec:java -Dexec.mainClass="com.sainsburys.test.scraper.ConsoleApplication"
```
