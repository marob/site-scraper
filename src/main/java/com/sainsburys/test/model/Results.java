package com.sainsburys.test.model;

import static java.math.BigDecimal.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * The results consisting of a list of products and the total prices.
 */
public class Results {

    private List<Product> results = new ArrayList<>();
    private BigDecimal total = ZERO;

    public void add(Product product) {
        results.add(product);
        total = total.add(product.getUnitPrice());
    }

    public List<Product> getResults() {
        return results;
    }

    public BigDecimal getTotal() {
        return total;
    }
}
