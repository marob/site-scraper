package com.sainsburys.test.model;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * The product with details scraped such as title and description.
 */
public class Product {

    private String title;
    private int size;
    private BigDecimal unitPrice;
    private String description;

    public Product(String title, int size, BigDecimal unitPrice, String description) {
        this.title = title;
        this.size = size;
        this.unitPrice = unitPrice;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getSize() {
        return (size / 1024) + "kb";
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(size, product.size) &&
                Objects.equals(title, product.title) &&
                Objects.equals(unitPrice, product.unitPrice) &&
                Objects.equals(description, product.description);
    }

    @Override
    public String toString() {
        return "Product{" +
                "\ntitle='" + title + '\'' +
                ",\nsize=" + size +
                ",\nunitPrice=" + unitPrice +
                ",\ndescription='" + description + '\'' +
                '}';
    }
}
