package com.sainsburys.test.scraper;

import static java.util.stream.Collectors.*;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * Extracts the webpage links of each product given in the grocery page URL.
 */
public class GroceryPageProductLinkExtractor {

    public List<String> extractProductLinksFrom(URL url) throws IOException {
        URLConnection connection = url.openConnection();
        Document document = Jsoup.parse(connection.getInputStream(), "UTF-8", url.toExternalForm());
        return extractProductLinksFrom(document);
    }

    private List<String> extractProductLinksFrom(Document document) throws IOException {
        return document.getElementsByClass("productTitle")
                .stream()
                .map(element -> element.getElementsByTag("a").attr("abs:href"))
                .collect(toList());
    }
}
