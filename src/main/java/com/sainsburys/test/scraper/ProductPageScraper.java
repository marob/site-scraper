package com.sainsburys.test.scraper;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import com.sainsburys.test.model.Product;

/**
 * The ProductPageScraper can consume the product webpage and present it as a product object.
 */
public class ProductPageScraper {

    public Product extractProductionFrom(URL url) throws IOException {
        byte[] data = IOUtils.toByteArray(url.openConnection().getInputStream());
        Document document = Jsoup.parse(new ByteArrayInputStream(data), "UTF-8", url.toExternalForm());

        int size = data.length;
        String title = document.select("strong[itemprop=name]").text();

        String description = "";
        Elements descriptionElements = document.select("#bopBottom > div:nth-child(2) > p");
        if (descriptionElements.size() > 0) {
            description = descriptionElements.first().text();
        }

        String price = document.select("#bopRight > div.productPrice > div > meta:nth-child(2)").attr("content");
        if (price.isEmpty()) {
            price = document.select("#bopRight > div.productPrice > div > meta:nth-child(3)").attr("content");
        }
        BigDecimal unitPrice = new BigDecimal(price);

        return new Product(title, size, unitPrice, description);
    }
}
