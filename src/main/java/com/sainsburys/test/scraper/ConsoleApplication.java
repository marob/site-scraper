package com.sainsburys.test.scraper;

import java.net.URL;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import com.sainsburys.test.model.Results;

/**
 * A simple console application that scrapes the Sainsbury’s grocery site - Ripe Fruits page
 * and prints a JSON array of all the products on the page.
 */
public class ConsoleApplication {

    private static final String GROCERY_SITE_URL = 
            "https://groceries.morrisons.com/webshop/getCategories.do?tags=%7C105651%7C104268%7C113968%7C113902%7C154698&viewAllProducts=true&index=0&view=text";

    public static void main(String[] args) throws Exception {
        GroceryListPageScraper consumer = new GroceryListPageScraper();
        Results results = consumer.consume(new URL(GROCERY_SITE_URL));

        ObjectWriter mapper = new ObjectMapper().writerWithDefaultPrettyPrinter();
        mapper.writeValue(System.out, results);
    }
}
