package com.sainsburys.test.scraper;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import com.sainsburys.test.model.Product;
import com.sainsburys.test.model.Results;

/**
 * The GroceryPageScraper can consume the grocery webpage, process the data and present it as
 * a list of products along with its description and unit price.
 */
public class GroceryListPageScraper {

    private GroceryPageProductLinkExtractor linkExtractor;
    private ProductPageScraper productPageScraper;

    public GroceryListPageScraper() {
        this.linkExtractor = new GroceryPageProductLinkExtractor();
        this.productPageScraper = new ProductPageScraper();
    }

    protected GroceryListPageScraper(GroceryPageProductLinkExtractor linkExtractor) {
        this.linkExtractor = linkExtractor;
        this.productPageScraper = new ProductPageScraper();
    }

    public Results consume(URL url) throws IOException {
        List<String> links = linkExtractor.extractProductLinksFrom(url);
        Results results = new Results();

        for (String link : links) {
            URL productUrl = new URL(link);
            Product product = productPageScraper.extractProductionFrom(productUrl);
            results.add(product);
        }

        return results;
    }
}
