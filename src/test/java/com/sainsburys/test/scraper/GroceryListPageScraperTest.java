package com.sainsburys.test.scraper;

import static java.util.Arrays.*;
import static org.hamcrest.core.Is.*;
import static org.hamcrest.core.IsEqual.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.List;

import org.junit.Test;
import com.sainsburys.test.model.Product;
import com.sainsburys.test.model.Results;

public class GroceryListPageScraperTest {

    @Test
    public void consume() throws IOException {
        URL url = this.getClass().getResource("/index.html");

        GroceryPageProductLinkExtractor linkExtractor = mock(GroceryPageProductLinkExtractor.class);
        List<String> productLinks = asList(
                this.getClass().getResource("/product1.html").toExternalForm(),
                this.getClass().getResource("/product2.html").toExternalForm());
        when(linkExtractor.extractProductLinksFrom(url)).thenReturn(productLinks);

        GroceryListPageScraper consumer = new GroceryListPageScraper(linkExtractor);
        Results results = consumer.consume(url);

        assertThat(results.getTotal(), is(equalTo(new BigDecimal("2.71"))));

        assertThat(results.getResults().get(0), is(equalTo(new Product(
                "Morrisons Mango Chunks", 118773, new BigDecimal("2.00"),
                "An 80g serving counts as 1 portion of your 5 a day fruit and vegetables."))));

        assertThat(results.getResults().get(1), is(equalTo(new Product(
                "Morrisons Kiwi", 105087, new BigDecimal("0.71"),
                "An 80g (2 kiwi fruit) serving counts as 1 portion of your 5 a day fruit and vegetables. " +
                        "Aim to eat at least 5 portions of different fruit and vegetables each day. " +
                        "Remember that fresh, frozen, dried, canned and juice all count!"))));
    }
}