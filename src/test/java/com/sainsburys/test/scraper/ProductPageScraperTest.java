package com.sainsburys.test.scraper;

import static org.hamcrest.core.Is.*;
import static org.hamcrest.core.IsEqual.*;
import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.net.URL;

import org.junit.Test;
import com.sainsburys.test.model.Product;

public class ProductPageScraperTest {

    private ProductPageScraper productPageScraper = new ProductPageScraper();

    @Test
    public void scrapeProduct1() throws Exception {
        URL url = this.getClass().getResource("/product1.html");
        Product product = productPageScraper.extractProductionFrom(url);

        assertThat(product, is(equalTo(new Product(
                "Morrisons Mango Chunks", 118773, new BigDecimal("2.00"),
                "An 80g serving counts as 1 portion of your 5 a day fruit and vegetables."))));
    }

    @Test
    public void scrapeProduct2() throws Exception {
        URL url = this.getClass().getResource("/product2.html");
        Product product = productPageScraper.extractProductionFrom(url);

        assertThat(product, is(equalTo(new Product(
                "Morrisons Kiwi", 105087, new BigDecimal("0.71"),
                "An 80g (2 kiwi fruit) serving counts as 1 portion of your 5 a day fruit and vegetables. " +
                        "Aim to eat at least 5 portions of different fruit and vegetables each day. " +
                        "Remember that fresh, frozen, dried, canned and juice all count!"))));
    }
}