package com.sainsburys.test.scraper;

import static org.hamcrest.core.Is.*;
import static org.hamcrest.core.IsEqual.*;
import static org.junit.Assert.*;

import java.net.URL;
import java.util.List;

import org.junit.Test;

public class GroceryPageProductLinkExtractorTest {

    private GroceryPageProductLinkExtractor linkExtractor = new GroceryPageProductLinkExtractor();

    @Test
    public void extractProductLinks() throws Exception {
        URL url = this.getClass().getResource("/index.html");

        List<String> productLinks = linkExtractor.extractProductLinksFrom(url);

        assertThat(productLinks.size(), is(equalTo(19)));

        assertTrue("Expected to contain link", productLinks.contains(
                "file:/webshop/product/Morrisons-Mango-Chunks/230951011?prevPageIndex=0&from=shop&" +
                        "tags=%7C105651%7C104268%7C113968%7C113902%7C154698&" +
                        "parentContainer=%7C104268%7C113968%7C113902%7C154698_TEXTVIEW"
        ));

        assertTrue("Expected to contain link", productLinks.contains(
                "file:/webshop/product/Morrisons-Pineapple/120567011?prevPageIndex=0&from=shop&" +
                        "tags=%7C105651%7C104268%7C113968%7C113902%7C154698&" +
                        "parentContainer=%7C104268%7C113968%7C113902%7C154698_TEXTVIEW"
        ));

        assertTrue("Expected to contain link", productLinks.contains(
                "file:/webshop/product/Morrisons-Hand-Selected-Giant-Mango/207824011?prevPageIndex=0&from=shop&" +
                        "tags=%7C105651%7C104268%7C113968%7C113902%7C154698&" +
                        "parentContainer=%7C104268%7C113968%7C113902%7C154698_TEXTVIEW"
        ));
    }
}